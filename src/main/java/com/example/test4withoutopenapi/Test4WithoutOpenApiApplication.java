package com.example.test4withoutopenapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Test4WithoutOpenApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(Test4WithoutOpenApiApplication.class, args);
    }

}
