package com.example.test4withoutopenapi.domain;

import com.example.test4withoutopenapi.model.FigureCalculation;
import com.example.test4withoutopenapi.model.FigureType;
import lombok.*;
import lombok.experimental.SuperBuilder;
import org.springframework.scheduling.annotation.EnableAsync;

import javax.annotation.PostConstruct;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.time.LocalDate;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Entity
@Getter
@Setter
@ToString
@DiscriminatorValue("CIRCLE")
@SuperBuilder(toBuilder = true)
public class Circle extends Figure  implements FigureCalculation {

    private Double radius = getParameters().get(0);

    @Override
    public Double setAreaTest() {
        return (Math.pow(radius, 2) * Math.PI);
    }

//    @Override
    public Double setParametersTest(List<Double> parameters) {
//        this.radius = parameters.get(0);
        return parameters.get(0);
    }

//    public Circle() {
//        this.radius = getParameters().get(0);
//    }

    @Override
    public Double setPerimeterTest() {
        return radius * 2 * Math.PI;
    }

    @Override
    public Double setParametersTest() {
        return radius;
    }

}
