package com.example.test4withoutopenapi.domain;

import com.example.test4withoutopenapi.model.FigureType;
import lombok.*;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Request {
    private FigureType figureType;
    private List<Double> parameters;
}
