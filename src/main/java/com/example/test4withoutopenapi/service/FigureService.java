package com.example.test4withoutopenapi.service;

import com.example.test4withoutopenapi.domain.Figure;
import com.example.test4withoutopenapi.domain.Request;
import com.example.test4withoutopenapi.model.FigureSearchCriteria;
import com.example.test4withoutopenapi.repository.FigureCriteriaRepository;
import com.example.test4withoutopenapi.repository.FigureRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@RequiredArgsConstructor
@Service
public class FigureService {
    private final FigureRepository figureRepository;
    private final FigureCriteriaRepository figureCriteriaRepository;

    //    public Figure save(Request request) {
//        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
//        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
//        )
//        if (request.getFigureType() == FigureType.CIRCLE) {
//            return figureRepository.save(Circle.builder()
//                    .figureType(FigureType.CIRCLE)
//                    .radius(request.getParameters().get(0))
//                    .version(1)
//                    .createdBy(userDetails.getUsername())
//                    .lastModifiedBy(userDetails.getUsername())
//                    .createdAt(LocalDate.now())
//                    .lastModifiedAt(LocalDate.now())
//                    .area(Math.pow(request.getParameters().get(0), 2) * Math.PI)
//                    .perimeter(request.getParameters().get(0) * Math.PI * 2)
//                    .build());
//        } else if (request.getFigureType() == FigureType.RECTANGLE) {
//            return figureRepository.save(Rectangle.builder()
//                    .figureType(FigureType.RECTANGLE)
//                    .width(request.getParameters().get(0))
//                    .height(request.getParameters().get(1))
//                    .version(1)
//                    .createdBy(userDetails.getUsername())
//                    .lastModifiedBy(userDetails.getUsername())
//                    .createdAt(LocalDate.now())
//                    .lastModifiedAt(LocalDate.now())
//                    .area(request.getParameters().get(0) * request.getParameters().get(1))
//                    .perimeter((request.getParameters().get(0) * 2) + (request.getParameters().get(1) * 2))
//                    .build());
//        } else
//            return figureRepository.save(Square.builder()
//                    .figureType(FigureType.SQUARE)
//                    .width(request.getParameters().get(0))
//                    .version(1)
//                    .createdBy(userDetails.getUsername())
//                    .lastModifiedBy(userDetails.getUsername())
//                    .createdAt(LocalDate.now())
//                    .lastModifiedAt(LocalDate.now())
//                    .area(Math.pow(request.getParameters().get(0), 2))
//                    .perimeter(request.getParameters().get(0) * 4)
//                    .build());
//    }
    public Figure save(Request request) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        return figureRepository.save(Figure.builder()
                .figureType(request.getFigureType())
                .version(1)
                .createdBy(userDetails.getUsername())
                .lastModifiedBy(userDetails.getUsername())
                .createdAt(LocalDate.now())
                .lastModifiedAt(LocalDate.now())
                .

        )
    }

    public Figure save(Figure figure) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();

        figure.setCreatedBy(userDetails.getUsername());
        figure.setLastModifiedBy(userDetails.getUsername());
//        figure.setVersion(1);
//        figure.setLastModifiedAt(LocalDate.now());
//        figure.setCreatedAt(LocalDate.now());
//        figure.setParametersTest(figure.getParameters());
//        figure.setArea(figure.setAreaTest());
//        figure.setPerimeter(figure.setPerimeterTest());
        return figureRepository.save(figure);
    }

    public List<Figure> getFigures(FigureSearchCriteria figureSearchCriteria) {
        return figureCriteriaRepository.figuresAfterFiler(figureSearchCriteria);
    }

}
