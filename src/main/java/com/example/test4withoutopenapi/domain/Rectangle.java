package com.example.test4withoutopenapi.domain;

import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Entity
@Getter
@Setter
@ToString
@DiscriminatorValue("RECTANGLE")
@SuperBuilder(toBuilder = true)
public class Rectangle extends Figure {
    private Double height;
    private Double width;


//    @Override
    public Double setAreaTest() {
        return null;
    }

//    @Override
    public Double setParametersTest(List<Double> parameters) {
        this.width = parameters.get(0);
        this.height = parameters.get(1);
        return 1.0;
    }

//    @Override
    public Double setPerimeterTest() {
        return null;
    }
}
