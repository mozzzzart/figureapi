package com.example.test4withoutopenapi.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;


@Configuration
@EnableWebSecurity
public class WebSecurityConfig {
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder(8);
    }

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http
                .csrf()
                .disable()
                .cors()
                .and()
                .authorizeRequests((authz) -> {
                            try {
                                authz
                                        .antMatchers(HttpMethod.POST, "/api/v1/figures/**").hasRole("CREATOR")
                                        .antMatchers(HttpMethod.GET,"/api/v1/figures/**").hasAnyRole("CREATOR", "USER")
                                        .antMatchers("/h2-console/**").permitAll()
                                        .anyRequest().authenticated()
                                        .and()
                                        .formLogin()
                                        .permitAll()
                                        .and()
                                        .logout()
                                        .permitAll();
                            } catch (Exception e) {
                                throw new RuntimeException(e);
                            }
                        }
                )
                .httpBasic(Customizer.withDefaults());
        return http.build();
    }
    @Bean
    public InMemoryUserDetailsManager userDetailsService() {
        UserDetails creator = User.builder()
                .username("andrzej")
                .password(passwordEncoder().encode("password123"))
                .roles("CREATOR")
                .build();
        UserDetails user = User.builder()
                .username("robol")
                .password(passwordEncoder().encode("password123"))
                .roles("USER")
                .build();
        return new InMemoryUserDetailsManager(creator, user);
    }
}
