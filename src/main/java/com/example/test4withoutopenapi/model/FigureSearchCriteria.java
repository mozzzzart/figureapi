package com.example.test4withoutopenapi.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class FigureSearchCriteria {
    private FigureType figureType;
    private Double areaFrom;
    private Double areaTo;
    private Double perimeterFrom;
    private Double perimeterTo;
    private Double radiusFrom;
    private Double radiusTo;
    private Double widthFrom;
    private Double widthTo;
    private Double heightFrom;
    private Double heightTo;
    private LocalDate createDateFrom;
    private LocalDate createDateTo;
    private String createdBy;
    private LocalDate lastModificationDateFrom;
    private LocalDate lastModificationDateTo;
    private String lastModificationBy;
}

