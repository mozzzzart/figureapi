package com.example.test4withoutopenapi.controller;

import com.example.test4withoutopenapi.domain.Figure;
import com.example.test4withoutopenapi.domain.Request;
import com.example.test4withoutopenapi.model.FigureSearchCriteria;
import com.example.test4withoutopenapi.repository.FigureRepository;
import com.example.test4withoutopenapi.service.FigureService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1")
@RequiredArgsConstructor
public class FigureController {
    private final FigureService figureService;
    private final FigureRepository figureRepository;
    @PostMapping("/figures")
    public ResponseEntity<Figure> save(@RequestBody Request figure){
        return new ResponseEntity<>(figureService.save(figure), HttpStatus.CREATED);
    }
    @GetMapping("/figures/")
    public ResponseEntity<List<Figure>> getShapes(FigureSearchCriteria figureSearchCriteria){
        return new ResponseEntity<>(figureService.getFigures(figureSearchCriteria), HttpStatus.OK);
    }
}
