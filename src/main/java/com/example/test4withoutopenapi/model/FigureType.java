package com.example.test4withoutopenapi.model;

public enum FigureType {
    CIRCLE, SQUARE, RECTANGLE
}
