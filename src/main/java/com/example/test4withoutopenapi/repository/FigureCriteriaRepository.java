package com.example.test4withoutopenapi.repository;

import com.example.test4withoutopenapi.domain.Circle;
import com.example.test4withoutopenapi.domain.Figure;
import com.example.test4withoutopenapi.domain.Rectangle;
import com.example.test4withoutopenapi.domain.Square;
import com.example.test4withoutopenapi.model.FigureSearchCriteria;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Repository
public class FigureCriteriaRepository {
    private final EntityManager entityManager;
    private final CriteriaBuilder criteriaBuilder;

    public FigureCriteriaRepository(EntityManager entityManager) {
        this.entityManager = entityManager;
        this.criteriaBuilder = entityManager.getCriteriaBuilder();
    }

    public List<Figure> figuresAfterFiler(FigureSearchCriteria figureSearchCriteria) {
        CriteriaQuery<Figure> criteriaQuery = criteriaBuilder.createQuery(Figure.class);
        Root<Figure> figureRoot = criteriaQuery.from(Figure.class);
        Root<Circle> circleRoot = criteriaBuilder.treat(figureRoot, Circle.class);
        Root<Rectangle> rectangleRoot = criteriaBuilder.treat(figureRoot, Rectangle.class);
        Root<Square> squareRoot = criteriaBuilder.treat(figureRoot, Square.class);
        Predicate predicate = getPredicate(figureSearchCriteria, figureRoot, circleRoot, rectangleRoot, squareRoot);
        criteriaQuery.where(predicate);
        return entityManager.createQuery(criteriaQuery).getResultList();
    }

    public Predicate getPredicate(FigureSearchCriteria figureSearchCriteria, Root<Figure> shapeRoot, Root<Circle> circleRoot, Root<Rectangle> rectangleRoot, Root<Square> squareRoot) {
        List<Predicate> predicates = new ArrayList<>();

        if (Objects.nonNull(figureSearchCriteria.getFigureType())) {
            predicates.add(criteriaBuilder.equal(shapeRoot.get("figureType"), figureSearchCriteria.getFigureType()));
        }
        if (Objects.nonNull(figureSearchCriteria.getCreatedBy())) {
            predicates.add(criteriaBuilder.equal(shapeRoot.get("createdBy"), figureSearchCriteria.getCreatedBy()));
        }
        if (Objects.nonNull(figureSearchCriteria.getLastModificationBy())) {
            predicates.add(criteriaBuilder.equal(shapeRoot.get("lastModifiedBy"), figureSearchCriteria.getLastModificationBy()));
        }
        if (Objects.nonNull(figureSearchCriteria.getCreateDateFrom()) && Objects.nonNull(figureSearchCriteria.getCreateDateTo())) {
            predicates.add(criteriaBuilder.between(shapeRoot.get("createdAt"), figureSearchCriteria.getCreateDateFrom(), figureSearchCriteria.getCreateDateTo()));
        }
        if (Objects.nonNull(figureSearchCriteria.getLastModificationDateFrom()) && Objects.nonNull(figureSearchCriteria.getLastModificationDateTo())) {
            predicates.add(criteriaBuilder.between(shapeRoot.get("lastModifiedAt"), figureSearchCriteria.getLastModificationDateFrom(), figureSearchCriteria.getLastModificationDateTo()));
        }
        if (Objects.nonNull(figureSearchCriteria.getAreaFrom()) && Objects.nonNull(figureSearchCriteria.getAreaTo())) {
            predicates.add(criteriaBuilder.between(shapeRoot.get("area"), figureSearchCriteria.getAreaFrom(), figureSearchCriteria.getAreaTo()));
        }
        if (Objects.nonNull(figureSearchCriteria.getPerimeterFrom()) && Objects.nonNull(figureSearchCriteria.getPerimeterTo())) {
            predicates.add(criteriaBuilder.between(shapeRoot.get("perimeter"), figureSearchCriteria.getPerimeterFrom(), figureSearchCriteria.getPerimeterTo()));
        }
        if (Objects.nonNull(figureSearchCriteria.getRadiusFrom()) && Objects.nonNull(figureSearchCriteria.getRadiusTo())) {
            predicates.add(criteriaBuilder.between(circleRoot.get("radius"), figureSearchCriteria.getRadiusFrom(), figureSearchCriteria.getRadiusTo()));
        }
        if (Objects.nonNull(figureSearchCriteria.getWidthFrom()) && Objects.nonNull(figureSearchCriteria.getWidthTo())) {
            predicates.add(criteriaBuilder.between(rectangleRoot.get("width"), figureSearchCriteria.getWidthFrom(), figureSearchCriteria.getWidthTo()));
        }
        if (Objects.nonNull(figureSearchCriteria.getHeightFrom()) && Objects.nonNull(figureSearchCriteria.getHeightTo())) {
            predicates.add(criteriaBuilder.between(rectangleRoot.get("height"), figureSearchCriteria.getHeightFrom(), figureSearchCriteria.getHeightTo()));
        }
        if (Objects.nonNull(figureSearchCriteria.getWidthFrom()) && Objects.nonNull(figureSearchCriteria.getWidthTo())) {
            predicates.add(criteriaBuilder.between(squareRoot.get("width"), figureSearchCriteria.getWidthFrom(), figureSearchCriteria.getWidthTo()));
        }
        return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
    }
}
