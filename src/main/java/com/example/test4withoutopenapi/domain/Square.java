package com.example.test4withoutopenapi.domain;

import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@Entity
@DiscriminatorValue("SQUARE")
@SuperBuilder
public class Square extends Figure {
    private Double width;


//    @Override
    public Double setAreaTest() {
        return null;
    }

//    @Override
    public Double setParametersTest(List<Double> parameters) {
        this.width = parameters.get(0);
        return null;
    }

//    @Override
    public Double setPerimeterTest() {
        return null;
    }
}