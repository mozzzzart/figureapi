package com.example.test4withoutopenapi.model;

public interface FigureCalculation {
    abstract Double setAreaTest();
    abstract Double setPerimeterTest();
    abstract Double setParametersTest();
}
