package com.example.test4withoutopenapi.repository;

import com.example.test4withoutopenapi.domain.Figure;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FigureRepository extends JpaRepository<Figure, Long> {
}
