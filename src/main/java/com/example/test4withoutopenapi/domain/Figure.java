package com.example.test4withoutopenapi.domain;

import com.example.test4withoutopenapi.model.FigureCalculation;
import com.example.test4withoutopenapi.model.FigureType;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import liquibase.pro.packaged.B;
import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.annotation.PostConstruct;
import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "type")
@JsonTypeInfo(
        include = JsonTypeInfo.As.EXISTING_PROPERTY,
        property = "figureType",
        use = JsonTypeInfo.Id.NAME,
        visible = true
)
@JsonSubTypes({
        @JsonSubTypes.Type(value = Square.class, name = "SQUARE"),
        @JsonSubTypes.Type(value = Circle.class, name = "CIRCLE"),
        @JsonSubTypes.Type(value = Rectangle.class, name = "RECTANGLE")
})
@SuperBuilder(toBuilder = true)
public class Figure implements FigureCalculation{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Enumerated(EnumType.STRING)
    private FigureType figureType;
    @ElementCollection
    private List<Double> parameters;
    @Builder.Default
    private Integer version = 1;
    private String createdBy;
    @Builder.Default
    private LocalDate createdAt = LocalDate.now();
    @Builder.Default
    private LocalDate lastModifiedAt = LocalDate.now();
    private String lastModifiedBy;
    private Double area;
    private Double perimeter;


    public Double setAreaTest(){
        return area;
    }


    public Double setPerimeterTest(){
        return perimeter;
    }

    @Override
    public Double setParametersTest() {
        return parameters.get(0);
    }


//    @PostConstruct
//    public abstract Double setAreaTest();

//    @PostConstruct
//    public abstract Double setParametersTest(List<Double> parameters);

//    public List<Double> getParameters() {
//        return parameters;
//    }

//    @PostConstruct
//    public abstract Double setPerimeterTest();

}
